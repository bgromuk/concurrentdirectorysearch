package com.company;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class CallableDirectoryThread implements Callable<List<Path>> {
    private Path path;

    public CallableDirectoryThread(Path path) {
        this.path = path;
    }

    @Override
    public List<Path> call() throws Exception {
        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(2);
        List<Future<List<Path>>> pathes = new ArrayList<>();

        Files.list(path)
                .filter(Files::isDirectory)
                .forEach(path -> {
                    CallableDirectoryThread calculator  = new CallableDirectoryThread(path);
                    Future<List<Path>> result = executor.submit(calculator);
                    pathes.add(result);
                });

        List<Path> result = new ArrayList<>();

        List<Path> fileList = Files.list(path)
                .filter(Files::isRegularFile)
                .collect(Collectors.toList());


        pathes.stream().forEach(listFuture -> {
            try {
                result.addAll(listFuture.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        });

        result.addAll(fileList);

        return result;
    }
}
